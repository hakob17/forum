<?php

namespace App\Controllers;

use App\Models\Messages;
use Lib\Controller;

/**
 * Class Home
 * Default Controller
 */
class Home extends Controller
{
    protected $data;

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $messages = new Messages();
        $this->data = $messages->select();
        $this->view->render('home/index', $this->data);
    }
}