<?php
/**
 * Created by PhpStorm.
 * User: Hakob
 * Date: 9/16/2016
 * Time: 6:30 AM
 */

namespace App\Models;


use Lib\Model;

class Messages extends Model
{
    protected $table = "messages";

    public function __construct()
    {
        parent::__construct();
    }
}