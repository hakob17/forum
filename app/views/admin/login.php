<div class="content container">
    <form class="col-lg-6 col-lg-offset-3" action="/auth/login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <div class="form-group">
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required=""
                   autofocus="">
        </div>
        <div class="form-group">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
        </div>
        <div class="form-group">
        <button class="btn btn-primary" type="submit">Sign in</button>
            <a class="btn btn-link" href="/auth/register">Don't have an account? Please Register</a>
        </div>
    </form>
</div>