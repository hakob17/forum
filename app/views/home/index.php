
<div class="to-form">
    <button id="to-form" class="btn btn-lg">Scroll to  Message</button>
</div>
<div class="container messages">
    <div class="panel-group">
        <?php foreach ($data as $message): ?>
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $message['name'] ?> ( <?php echo $message['email'] ?> )</div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <img src="<?php echo $message['image_url'] ?>" alt="Here is the image" width="240" height="320">
                    </div>
                    <div class="col-md-8">
                        <?php echo $message['message'] ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div  class="col-md-8 col-md-offset-2 message-form-holder">
        <form id="message-form" enctype="multipart/form-data" method="post">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input class="form-control" type="text" name="name" id="name" required>
            </div>
            <div class="form-group  col-md-6">
                <label for="email">Email</label>
                <input class="form-control" type="email" name="email" id="email" required>
            </div>
            <div class="form-group  col-md-12">
                <label for="message">Message</label>
                <textarea  class="form-control" name="message" id="message" required rows="10"></textarea>
            </div>
            <div class="form-group  col-md-4">
                <label for="">Add Image</label>
                <label class="btn btn-default btn-file form-control">
                    Browse <input type="file" id="image" style="display: none;">
                </label>
            </div>
            <div class="form-group  col-md-4">
                <button class="btn btn-primary form-control button-label" id="preview">Preview</button>
            </div>
            <div class="form-group  col-md-4">
                <button class="btn btn-success form-control button-label submit-form">Send</button>
            </div>
        </form>
    </div>
</div>

<div id="message-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Preview</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading preview-header"> (  )</div>
                    <div class="panel-body preview-body">
                        <div class="col-md-3 preview-body-image">
                            <img src="" alt="Here is the image" width="200" height="300">
                        </div>
                        <div class="col-md-8 preview-body-message">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success submit-form" data-dismiss="modal">Send</button>
            </div>
        </div>

    </div>
</div>