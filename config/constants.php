<?php


define('APP', PUBLIC_DIR.'/../app/');
define('ROOT', PUBLIC_DIR.'/../');
define('CONF_PATH', PUBLIC_DIR.'/../config/');
define('LIB', PUBLIC_DIR.'/../lib/');
define('VIEWS', APP.'/views/');
define('CONTROLLERS', APP.'/controllers/');
