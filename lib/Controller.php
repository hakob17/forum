<?php

namespace Lib;
/**
 * Class Controller
 * Controller Class
 * All Controllers should extend this
 */

class Controller
{
    protected $view;

    public function __construct(){
        $this->view = new View();
    }

    public function loadModel($name){
        $path = APP . '/models/' . $name . '_model.php';

        if(file_exists($path)){
            require APP . '/models/' . $name . '_model.php';
            $modelName = $name . 'Model';
            $this->model = new $modelName();
        }
    }

    protected function cleanInput( $input ) {
        $input = strip_tags( $input );
        $input = htmlentities( $input );

        return $input;
    }
}