<?php

namespace Lib;
use PDO as PDO;

class DB
{
    /**
     * @var PDO
     * Instance of our DB class
     */
    private static $instance;
    private static $pdo_instance;

    /**
     * @return Database|PDO
     * static method to get our object outside
     * used for singleton pattern
     */
    public static function getInstance()
    {
        if (!self::$instance) { // If no instance then make one
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getPdoInstance() {
        return self::$pdo_instance;
    }
    /**
     * Database constructor.
     * Private constructor for not being called from outside
     */
    private function __construct()
    {
        $this->connect();
    }

    private function connect()
    {
        try {
            // assign PDO object to db variable
            self::$pdo_instance = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));

            self::$pdo_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            self::$pdo_instance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        } catch (\PDOException $e) { //Output error – would normally log this to error file rather than output to user.
            echo $e->getMessage();
            die();
        }
    }

    /**     *
     * prevents object instance cloning, so the object is one at request lifetime
     */
    private function __clone()
    {
    }

    /**
     * Closes the PDO connection
     */
    public function closeConnection()
    {
        self::$pdo_instance = null;
    }


}