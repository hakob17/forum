<?php

/**
 * Class Database
 * Database class
 * used Singletone design pattern
 */
require("Log.php");

//class Database
//{
//    # @object, PDO statement object
//    private $sQuery;
//
//    # @array,  The database settings
//    private $settings;
//
//    # @bool ,  Connected to the database
//    private $bConnected = false;
//
//    # @object, Object for logging exceptions
//    private $log;
//
//    # @array, The parameters of the SQL query
//    private $parameters;
//
//    /**
//     * @var PDO
//     * Instance of our DB class
//     */
//    private static $pdo_instance;
//
//    /**
//     * @return Database|PDO
//     * static method to get our object outside
//     * used for singletone pattern
//     */
//    public static function getInstance()
//    {
//        if (!self::$_instance) { // If no instance then make one
//            self::$_instance = new self();
//        }
//        return self::$_instance;
//    }
//
//    /**
//     * Database constructor.
//     * Private constructor for not being called from outside
//     */
//    private function __construct()
//    {
//        $this->log = new Log();
//        $this->connect();
//        $this->parameters = array();
//    }
//
//    private function connect()
//    {
//        try {
//            // assign PDO object to db variable
//            self::$pdo_instance = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, array(
//                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
//            ));
//
//            self::$pdo_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//
//            self::$pdo_instance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//
//            $this->bConnected = true;
//
//        } catch (PDOException $e) { //Output error – would normally log this to error file rather than output to user.
//            echo $this->ExceptionLog($e->getMessage());
//            die();
//        }
//    }
//
//    /**     *
//     * prevents object instance cloning, so the object is one at request lifetime
//     */
//    private function __clone()
//    {
//    }
//
//    /**
//     * Closes the PDO connection
//     */
//    public function CloseConnection()
//    {
//        $this->pdo_instance = null;
//    }
//
//    /**
//     *    Every method which needs to execute a SQL query uses this method.
//     *
//     *    1. If not connected, connect to the database.
//     *    2. Prepare Query.
//     *    3. Parameterize Query.
//     *    4. Execute Query.
//     *    5. On exception : Write Exception into the log + SQL query.
//     *    6. Reset the Parameters.
//     */
//    private function Init($query, $parameters = "")
//    {
//        # Connect to database
//        if (!$this->bConnected) {
//            $this->connect();
//        }
//        try {
//            # Prepare query
//            $this->sQuery = $this->pdo_instance->prepare($query);
//
//            # Add parameters to the parameter array
//            $this->bindMore($parameters);
//
//            # Bind parameters
//            if (!empty($this->parameters)) {
//                foreach ($this->parameters as $param => $value) {
//
//                    $type = PDO::PARAM_STR;
//                    switch ($value[1]) {
//                        case is_int($value[1]):
//                            $type = PDO::PARAM_INT;
//                            break;
//                        case is_bool($value[1]):
//                            $type = PDO::PARAM_BOOL;
//                            break;
//                        case is_null($value[1]):
//                            $type = PDO::PARAM_NULL;
//                            break;
//                    }
//                    // Add type when binding the values to the column
//                    $this->sQuery->bindValue($value[0], $value[1], $type);
//                }
//            }
//
//            # Execute SQL
//            $this->sQuery->execute();
//        } catch (PDOException $e) {
//            # Write into log and display Exception
//            echo $this->ExceptionLog($e->getMessage(), $query);
//            die();
//        }
//
//        # Reset the parameters
//        $this->parameters = array();
//    }
//
//    /**
//     * @void
//     *
//     *    Add the parameter to the parameter array
//     * @param string $para
//     * @param string $value
//     */
//    public function bind($para, $value)
//    {
//        $this->parameters[sizeof($this->parameters)] = [":" . $para, $value];
//    }
//
//    /**
//     * @void
//     *
//     *    Add more parameters to the parameter array
//     * @param array $parray
//     */
//    public function bindMore($parray)
//    {
//        if (empty($this->parameters) && is_array($parray)) {
//            $columns = array_keys($parray);
//            foreach ($columns as $i => &$column) {
//                $this->bind($column, $parray[$column]);
//            }
//        }
//    }
//
//    /**
//     *  If the SQL query  contains a SELECT or SHOW statement it returns an array containing all of the result set row
//     *    If the SQL statement is a DELETE, INSERT, or UPDATE statement it returns the number of affected rows
//     *
//     * @param  string $query
//     * @param  array $params
//     * @param  int $fetchmode
//     * @return mixed
//     */
//    public function query($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
//    {
//        $query = trim(str_replace("\r", " ", $query));
//
//        $this->Init($query, $params);
//
//        $rawStatement = explode(" ", preg_replace("/\s+|\t+|\n+/", " ", $query));
//
//        # Which SQL statement is used
//        $statement = strtolower($rawStatement[0]);
//
//        if ($statement === 'select' || $statement === 'show') {
//            return $this->sQuery->fetchAll($fetchmode);
//        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
//            return $this->sQuery->rowCount();
//        } else {
//            return NULL;
//        }
//    }
//
//    /**
//     *  Returns the last inserted id.
//     * @return string
//     */
//    public function lastInsertId()
//    {
//        return $this->pdo_instance->lastInsertId();
//    }
//
//    /**
//     * Starts the transaction
//     * @return boolean, true on success or false on failure
//     */
//    public function beginTransaction()
//    {
//        return $this->pdo_instance->beginTransaction();
//    }
//
//    /**
//     *  Execute Transaction
//     * @return boolean, true on success or false on failure
//     */
//    public function executeTransaction()
//    {
//        return $this->pdo_instance->commit();
//    }
//
//    /**
//     *  Rollback of Transaction
//     * @return boolean, true on success or false on failure
//     */
//    public function rollBack()
//    {
//        return $this->pdo_instance->rollBack();
//    }
//
//    /**
//     *    Returns an array which represents a column from the result set
//     *
//     * @param  string $query
//     * @param  array $params
//     * @return array
//     */
//    public function column($query, $params = null)
//    {
//        $this->Init($query, $params);
//        $Columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);
//
//        $column = null;
//
//        foreach ($Columns as $cells) {
//            $column[] = $cells[0];
//        }
//
//        return $column;
//
//    }
//
//    /**
//     *    Returns an array which represents a row from the result set
//     *
//     * @param  string $query
//     * @param  array $params
//     * @param  int $fetchmode
//     * @return array
//     */
//    public function row($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
//    {
//        $this->Init($query, $params);
//        $result = $this->sQuery->fetch($fetchmode);
//        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued,
//        return $result;
//    }
//
//    /**
//     *    Returns the value of one single field/column
//     *
//     * @param  string $query
//     * @param  array $params
//     * @return string
//     */
//    public function single($query, $params = null)
//    {
//        $this->Init($query, $params);
//        $result = $this->sQuery->fetchColumn();
//        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued
//        return $result;
//    }
//
//    /**
//     * Writes the log and returns the exception
//     *
//     * @param  string $message
//     * @param  string $sql
//     * @return string
//     */
//    private function ExceptionLog($message, $sql = "")
//    {
//        $exception = 'Unhandled Exception. <br />';
//        $exception .= $message;
//        $exception .= "<br /> You can find the error back in the log.";
//
//        if (!empty($sql)) {
//            # Add the Raw SQL to the Log
//            $message .= "\r\nRaw SQL : " . $sql;
//        }
//        # Write into log
//        $this->log->write($message);
//
//        return $exception;
//    }
//}

?>