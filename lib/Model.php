<?php

namespace Lib;

class Model {

    protected $table;

    protected $fields = [];

    protected $db;

    public function __construct(){
        $this->db = DB::getInstance();
    }

    public function select(array $what_to_select = ['*'])
    {
        $whats = "";
        $wheres = "";

        for ($i = 0; $i < count($what_to_select); $i++) {
            if ($i != count($what_to_select) - 1) {
                $whats .= $what_to_select[$i] . ", ";
            } else {
                $whats .= $what_to_select[$i] . " ";

            }
        }
        $query_string = "SELECT " . $whats . "FROM " . $this->table;

        $sth = $this->db->getPdoInstance()->prepare($query_string);
        $sth->execute();

        return $sth->fetchAll(\PDO::FETCH_ASSOC);

    }
}