<?php

namespace Lib;
use App\Controllers\Home;

/**
 * Class Init
 *  Class to Initialize Connection
 */
class Router
{

    public function __construct()
    {

        $url = isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : null;
        $url = trim($url, '/'); //right trim (remove characters from the right side of the string)
        $url = explode('/', $url);

        if (empty($url[0])) {
            require APP . 'controllers/Home.php';
            $con = new Home();
            $con->loadModel('home');
            $con->index();

            return false;
        }

        $file = APP . '/controllers/' . ucfirst($url[0]) . '.php';

        if (file_exists($file)) {
            require $file;
        } else {
            $this->error('File does not exist.');
        }

        $this->detectMethod($_SERVER["REQUEST_METHOD"], $url);
    }

    public function error($error)
    {
        require APP . '/controllers/Error.php';
        echo '<pre>';
        print_r($error);
        die;
        $error = new Error($error);
        $error->index();
        return false;
    }

    private function detectMethod($requestMethod, array $url)
    {
        switch ($requestMethod) {
            case "GET":
                $this->getMethod($url);
                break;
            case "POST":
                $this->postMethod($url);
                break;

        }
    }

    private function getMethod(array $url)
    {
        $controller = new ucfirst($url[0]);

        $controller->loadModel($url[0]);

        if(isset($url[1])) {
            $method = 'get' . ucfirst($url[1]);
        }

        if (isset($url[2])) {
            if (method_exists($controller, $method)) {
                $controller->{$method}($url[2]);
            } else {
                $this->error('Method does not exist.');
            }
        } else {
            if (isset($url[1])) {
                if (method_exists($controller, $method)) {
                    $controller->{$method}();
                } else {
                    $this->error('Not Found.');
                }
            } else {
                $controller->index();
            }
        }
    }

    private function postMethod(array $url)
    {
        $ctrl = ucfirst($url[0]);
        $controller = new $ctrl;

        $controller->loadModel($url[0]);

        if(isset($url[1])) {
            $method = 'post' . ucfirst($url[1]);
        }

        $data = $_POST;

        if (isset($url[1])) {
            if (method_exists($controller, $method)) {
                $controller->{$method}($data);
            } else {
                $this->error('Not Found.');
            }
        } else {
            $controller->index();
        }
    }
}