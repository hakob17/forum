<?php

namespace Lib;

/**
 * Class View
 * View Generator
 */
class View
{
    public function render($name, $data)
    {
        require VIEWS . '/layout/header.php';
        require VIEWS . '/' . $name . '.php';
        require VIEWS . '/layout/footer.php';
    }
}