<?php


define('PUBLIC_DIR', dirname(realpath(__FILE__)));
# Configuration
require_once PUBLIC_DIR . '/../config/config.php';
require_once PUBLIC_DIR . '/../config/constants.php';



// Using autloader, add necessary files for function
spl_autoload_register( function ($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    var_dump($fileName);
    if(!($fileName == "Lib".DIRECTORY_SEPARATOR."PDO.php") ) {
        require ROOT.$fileName;
    }
});

$init = new \Lib\Router();