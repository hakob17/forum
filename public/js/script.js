$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
$('#to-form').click(function () {
    $('html, body').animate({
        scrollTop: $("#message-form").offset().top
    }, 1500);
})

$('form').submit(function (e) {
    e.preventDefault();
})

$('#preview').click(function (e) {
    e.preventDefault();
    if($('#message-form').valid()) {
        $('#message-modal').find('.preview-header').text($('#name').val() + ' ( '+ $('#email').val() + ' )')
        $('#message-modal').find('.preview-body-message').text($('#message').val())
        $('#message-modal').modal();
    }
})

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#message-modal').find('.preview-body-image img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function(){
    readURL(this);
});

$('.submit-form').click(function () {

    var formData = new FormData($('#message-form')[0]);

    $.ajax({
        url: '/message/add',
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            alert(data)
        }
    });

    return false;
})